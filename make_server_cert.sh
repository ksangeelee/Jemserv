#!/bin/bash

KEYSTORE="keystore.jks"
PASSWORD="password"

# Place the primary host at the start of the list.
HOSTS=("localhost" "debian-vm1")

# Form the Server Alternative Names list
for HOST in ${HOSTS[*]}; do
    SAN="${SAN},dns:${HOST}"
done

SAN=${SAN#,}

echo $SAN

rm keystore.jks

echo -ne "${HOSTS[1]}\n\n\n\n\n\nyes\n" | \
    keytool \
        -genkey \
        -alias selfsigned \
        -keyalg RSA \
        -keystore ${KEYSTORE} \
        -storepass "${PASSWORD}" \
        -validity 3650 \
        -ext "san=${SAN}" \
        -keysize 2048

echo "Done, verify the keystore details below"

keytool -list -v -keystore ${KEYSTORE} -storepass "${PASSWORD}"
