package net.susa;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;

public class Utils {

	public static void silentClose(OutputStream os) {

		if (os != null) {
			try {
				os.close();
			} catch (IOException ignore) {
			}
		}
	}

	public static void silentClose(Socket s) {

		if (s != null) {
			try {
				s.close();
			} catch (IOException ignore) {
			}
		}
	}

	public static JemservHost virtualHostFromUrl(URL requestUrl, ArrayList<JemservHost> hosts) {
		
		String hostname = requestUrl.getHost();
		
		for (int i = 0; i < hosts.size(); i++) {
			JemservHost host = hosts.get(i); 
			if(host.hasHostname(hostname)) {
				return host;
			}
		}
		return null;
	}
}
