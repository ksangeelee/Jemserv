package net.susa;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

public class GeminiRequestHandler {

	public static GeminiResponse processUrl(GeminiRequest req, JemservHost host) throws IOException {

		System.out.println("Reading resource for URL: " + req.requestUrl);

		URL url = req.requestUrl;

		String path = url.getPath();
		String query = url.getQuery();

		System.out.println("Protocol: " + url.getProtocol());
		System.out.println("Host: " + url.getHost());
		System.out.println("Port: " + url.getPort() + ", default is " + url.getDefaultPort());
		System.out.println("Path: " + path);
		System.out.println("Query: " + query);

		if (path.isEmpty()) {
			return GeminiResponse.redirect(url.toExternalForm() + "/");
		}

		String filePath = host.getDocRoot() + path;
		File f = new File(filePath);

		String cgiDir = host.getCgiDir();
		
		if (cgiDir != null && path.startsWith(cgiDir) && f.canExecute()) {

			// If the path doesn't refer to a file, then it can never be executed.
			if (!f.isFile()) {
				return GeminiResponse.permanentFailure(GeminiResponse.NOT_FOUND);
			}

			System.out.println("Executing CGI: " + filePath);

			byte[] cgiResponse = CgiLauncher.launchCgiProcess(f.getCanonicalPath(), req);

			GeminiResponse geminiResponse = new GeminiResponse(0, cgiResponse);

			return geminiResponse;
		}

		// Test for a regular file or directory to send...
		if (f.exists()) {

			System.out.println("File(\"" + filePath + "\") exists.");

			if (f.isFile()) {

				System.out.println("    and is a regular file.");

				return getFileResponse(f);
			}

			if (f.isDirectory()) { // then look for an index file

				System.out.println("    and is a directory.");

				if (!filePath.endsWith("/")) {
					System.out.println("    with no trailing '/'. Redirecting to correct.");
					return GeminiResponse.redirect(url.toExternalForm() + "/");
				}

				File idxf = new File(filePath + "index.gmi");

				if (idxf.exists() && idxf.isFile()) {

					System.out.println("    and contains and index file.");

					return getFileResponse(idxf);

				} else { // no index file, so senddirectory listing

					System.out.println("    and has no index file. Listing directory.");

					return getDirectoryListing(f);
				}
			}
		} // end if(file.exists())

		// 51 NOT FOUND
		return GeminiResponse.permanentFailure(GeminiResponse.NOT_FOUND);
	}

	private static GeminiResponse getFileResponse(File f) throws FileNotFoundException, IOException {

		FileInputStream fin = new FileInputStream(f);

		byte[] buffer = fin.readAllBytes();
		fin.close();

		MimeType mimeType = MimeType.getByFilename(f.getName());

		return new GeminiResponse(20, buffer, mimeType);
	}

	private static GeminiResponse getDirectoryListing(File f) throws FileNotFoundException, IOException {

		StringBuilder strb = new StringBuilder(1000);

		File[] dir = f.listFiles();

		for (File file : dir) {
			strb.append("=> ").append(file.getName()).append('\n');
		}

		return new GeminiResponse(20, strb.toString().getBytes(), MimeType.gmi);
	}
}
