/**
 * Released under the GNU GPL Version 3. See the license at:
 *   https://gitlab.com/ksangeelee/Jemserv/-/blob/master/LICENSE
 * Please do not use this software unless you agree to the terms
 * of this license.
 */
package net.susa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.ExtendedSSLSession;
import javax.net.ssl.SNIServerName;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

public class Jemserv implements Runnable {

	private SSLServerSocket serverSocket = null;
	private ArrayList<JemservHost> hosts;

	protected Jemserv(SSLServerSocket ss, ArrayList<JemservHost> hosts) {

		this.hosts = hosts;
		this.serverSocket = ss;

		newListener(); // perhaps move this to the main method?
	}

	/**
	 * The "listen" thread that accepts a connection to the server.
	 */
	public void run() {

		SSLSocket socket;

		// accept a connection
		try {
			socket = (SSLSocket) serverSocket.accept();
		} catch (IOException e) {
			System.out.println("Class Server died: " + e.getMessage());
			e.printStackTrace();
			return;
		}

		/*
		 * Create a new thread to accept() a new connection.
		 */
		newListener(); // effectively re-invokes this run() method

		/*
		 * Now carry on in this thread to process our accepted socket.
		 */

		GeminiRequest req = new GeminiRequest();

		req.serverPort = socket.getLocalPort();

		InetSocketAddress remoteAddr = (InetSocketAddress) socket.getRemoteSocketAddress();

		req.remoteAddr = remoteAddr.getAddress().getHostAddress();
		req.remotePort = remoteAddr.getPort();

		socket.addHandshakeCompletedListener(JemservSSL.getHandshakeListener(req));

		try {
			// This will be used to send the response to the request, or any protocol errors
			// that arise.
			OutputStream rawOut = socket.getOutputStream();

			try {
				// get path to class file from header
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

				String requestedUrl;

				if ((requestedUrl = in.readLine()) == null) {
					// e.g. client is prompting user to trust certificate
					System.out.println("Null request, returning now");
					return;
				}

				System.out.println("Got requet for '" + requestedUrl + "'");
				System.out.println("Client sends auth '" + req.signatureHash + "'");

				req.requestUrl = new URL(requestedUrl);

				JemservHost host = Utils.virtualHostFromUrl(req.requestUrl, hosts);

				if (host == null) {
					host = hosts.get(0); // assume we have a default host
				}

				/*
				 * The following block fetches the SNI name and checks it against the host
				 * specified in the request URL. Nothing is done with this at the moment, but it
				 * may be required to prioritise one over the other. SNI is mandated by the
				 * specification, but no real detail is given.
				 */
				SSLSession s = socket.getSession();
				List<SNIServerName> sniNames = null;
				if (s instanceof ExtendedSSLSession) {
					sniNames = ((ExtendedSSLSession) s).getRequestedServerNames();
					if (sniNames != null && sniNames.size() > 0) {
						for (Iterator<SNIServerName> iterator = sniNames.iterator(); iterator.hasNext();) {
							SNIServerName sni = (SNIServerName) iterator.next();
							String sniHost = new String(sni.getEncoded());
							if (host.hasHostname(sniHost)) {
								System.out.println("SNI host matches URL host.");
							} else {
								System.out.println("SNI host '" + sniHost + "' does not match the URL host.");
							}
						}
					}
				} // End of SNI check block

				/*
				 * Process the request via GeminiRequestHandler
				 */
				GeminiResponse resp = GeminiRequestHandler.processUrl(req, host);

				if (resp == null) { // dev-mode: throw an exception to shut the server for us!
					throw new RuntimeException("Null response from processUrl. This should not happen");
				}

				/*
				 * Handle the response by writing the output according to the response code.
				 */
				handleResponse(rawOut, resp);

			} catch (SSLHandshakeException he) {

				// e.g. Unrecognized Server Name Indication (SNI)
				System.out.println(he.getMessage());

				rawOut.close();

			} catch (Exception e) {
				System.out.println("I caught an exception.");
				e.printStackTrace();
				// write out error response
				rawOut.write(("40 TEMPORARY FAILURE: " + e.getMessage() + "\r\n").getBytes());
				rawOut.flush();
			}

		} catch (IOException ex) {
			System.out.println("error writing response: " + ex.getMessage());
			ex.printStackTrace();

		} finally {
			Utils.silentClose(socket);
		}
	}

	private void handleResponse(OutputStream rawOut, GeminiResponse resp) {
		/*
		 * Handle the GeminiResponse. This block needs to be refactored, though not sure
		 * which way to do it best - defer until more response codes have been
		 * implemented.
		 */
		try {

			switch (resp.responseCode) {
			case 0: // The header is contained in the response (e.g. CGI)
				rawOut.write(resp.content);
				break;
			case 20: // SUCCESS
				rawOut.write(resp.getResponseHeader());
				rawOut.write(resp.content);
				break;
			case 30: // REDIRECT TEMPORARY
			case 31: // REDIRECT PERMANENT
			case 51: // NOT FOUND
				byte[] responseHeader = resp.getResponseHeader();
				rawOut.write(responseHeader);
				break;
			default:
				break;
			}

			rawOut.flush();

		} catch (IOException ioe) {
			System.out.println("IOException while responding with code " + resp.responseCode + ": " + ioe.getMessage());
		}
	}

	/**
	 * Method to create a new thread to listen.
	 */
	private void newListener() {
		(new Thread(this)).start();
	}

	/**
	 * We have to specifically set a URLStreamHandler if we want to use the URL
	 * class to parse gemini URLs.
	 */
	static {
		URL.setURLStreamHandlerFactory(protocol -> "gemini".equals(protocol) ? new URLStreamHandler() {

			@Override
			protected URLConnection openConnection(URL url) throws IOException {
				return new URLConnection(url) {
					public void connect() throws IOException {
						System.out.println("Connected!");
					}
				};
			}

			@Override
			protected int getDefaultPort() {
				return 1965;
			}

		} : null);
	}

	public static void main(String args[]) throws Exception {

		ArrayList<JemservHost> hosts = new ArrayList<JemservHost>();

		JemservHost host1 = new JemservHost("localhost", "docroot");
		host1.setCgiDir("/cgi-bin");

		ServerConfig conf = new ServerConfig();

		hosts.add(host1);

		JemservHost host2 = new JemservHost("debian-vm1", "docroot/host2");

		hosts.add(host2);

		int geminiPort = conf.getGeminiPort();

		SSLServerSocketFactory socketFactory = JemservSSL.getSSLServerSocketFactory(conf);

		SSLServerSocket serverSocket = (SSLServerSocket) socketFactory.createServerSocket(geminiPort);

		InetAddress address = serverSocket.getInetAddress();

		System.out.println("Hostname " + address.getCanonicalHostName());
		System.out.println("Host address: " + address.getHostAddress());
		System.out.println("Running on port: " + serverSocket.getLocalPort());

		serverSocket.setWantClientAuth(true);

		new Jemserv(serverSocket, hosts);
	}
}
