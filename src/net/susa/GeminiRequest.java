package net.susa;

import java.net.URL;

/**
 * This is currently just a container for variables required to provide the CGI
 * environment, though doubtless useful elsewhere. I'm using public attributes
 * for primitives as a bit of an experiment to see how plays out - it may change
 * in furure. Getters will only be used when getting attributes of objects.
 * 
 * @author Kevin Sangeelee
 *
 */
public class GeminiRequest {

	public URL requestUrl;

	public int serverPort;

	public String remoteAddr;
	public int remotePort;

	public String authType;
	public String subjectCn;
	public String signatureHash;

	public GeminiRequest() {
	}

	public GeminiRequest(URL url) {
		this.requestUrl = url;
	}

	public String getQueryString() {
		return requestUrl.getQuery();
	}

	public String getPathInfo() {
		return requestUrl.getPath();
	}

	public void setIdParams(String subjectCn, String signatureHash, String authType) {

		this.subjectCn = subjectCn;
		this.signatureHash = signatureHash;
		this.authType = authType;
	}
}
