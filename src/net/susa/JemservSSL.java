package net.susa;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import javax.net.ssl.HandshakeCompletedEvent;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Tries to encapsulate code directly related to SSL, to keep it away from the
 * main application.
 * 
 * @author Kevin Sangeelee
 *
 */
public class JemservSSL {

	public static X509TrustManager getTrustManager() {

		return new X509TrustManager() {

			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
				System.out.println("checkServerTrusted: " + certs);
				System.out.println("          authType: " + authType);
			}
		};
	}

	public static String idFromCertificate(X509Certificate cert, GeminiRequest env) {

		String signatureHash = null;
		String subjectCn = null;

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");

			byte[] digest = md.digest(cert.getSignature());

			String subjectDn = cert.getSubjectDN().getName();
			String[] subjectParts = subjectDn.split(",");

			for (String sp : subjectParts) {
				if (sp.startsWith("CN="))
					subjectCn = sp.substring(3);
			}
			signatureHash = new String(sha256BytesToHex(digest));

		} catch (NoSuchAlgorithmException nsae) {
			throw new RuntimeException("Can't function without SHA-256");
		}

		env.setIdParams(subjectCn, signatureHash, cert.getType());

		return signatureHash;
	}

	/**
	 * The HandshakeCompletedListener is called when the SSL handshake has
	 * completed. We set this on the socket to access the certificate information
	 * (e.g. to get the signature hash for CGI).
	 */
	protected static HandshakeCompletedListener getHandshakeListener(GeminiRequest env) {

		return new HandshakeCompletedListener() {

			@Override
			public void handshakeCompleted(HandshakeCompletedEvent e) {
				try {
					Certificate[] certs = e.getPeerCertificates();

					for (Certificate certificate : certs) {

						if (certificate instanceof X509Certificate) {
							JemservSSL.idFromCertificate((X509Certificate) certificate, env);
						}
					}
				} catch (SSLPeerUnverifiedException ignore) {
					// We don't *require* a certificate, we just support
					// it if it's provided...
				}

			}
		};
	}

	protected static SSLServerSocketFactory getSSLServerSocketFactory(ServerConfig config) {

		SSLServerSocketFactory serverSocketFactory = null;

		try {
			// set up key manager to do server authentication
			SSLContext sslContext;

			KeyManagerFactory keyManagerFactory;
			KeyStore keyStore;

			char[] passphrase = config.getGeminiKeystorePassword();

			sslContext = SSLContext.getInstance("TLS");

			keyStore = KeyStore.getInstance("JKS");
			keyStore.load(new FileInputStream(config.getGeminiKeystore()), passphrase);

			keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
			keyManagerFactory.init(keyStore, passphrase);
			KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();

			X509TrustManager trustManager = JemservSSL.getTrustManager();
			TrustManager[] trustManagers = new TrustManager[] { trustManager };

			sslContext.init(keyManagers, trustManagers, null);

			serverSocketFactory = sslContext.getServerSocketFactory();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return serverSocketFactory;
	}

	private static final char[] HEX = "0123456789ABCDEF".toCharArray();

	/*
	 * Generate a string of hex for CGI purposes. Assumes the byte array is an
	 * SHA-256 hash (exactly 32 bytes), exception thrown otherwise!
	 */
	public static String sha256BytesToHex(byte[] bytes) {

		StringBuilder strb = new StringBuilder(64);

		for (int idx = 0; idx < 32; idx++) {
			strb.append(HEX[(bytes[idx] >>> 4) & 0xf]);
			strb.append(HEX[bytes[idx] & 0xf]);
		}
		return strb.toString();
	}

}
