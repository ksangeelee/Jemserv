package net.susa;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class CgiLauncher {

	public static byte[] launchCgiProcess(String command, GeminiRequest req) throws IOException {

		// ProcessBuilder gives us easier redirection of stderr than Runtime.
		// "/bin/python2.7", "-m", "justext", "-s", "English"
		ProcessBuilder pb = new ProcessBuilder(command);

		pb.redirectInput(new File("/dev/null"));
		
		Map<String, String> env = pb.environment();
		env.clear(); // clear entirely for now, perhaps loosen this in future.

		env.put("GEMINI_URL", req.requestUrl.toExternalForm());
		env.put("SERVER_NAME", "localhost");
		env.put("SERVER_PROTOCOL", "Gemini");
		env.put("SERVER_SOFTWARE", "Jemserv");
		env.put("SCRIPT_NAME", req.requestUrl.getPath());
		env.put("SERVER_PORT", String.valueOf(req.serverPort));

		env.put("REMOTE_ADDR", req.remoteAddr);
		env.put("REMOTE_HOST", req.remoteAddr);
		env.put("REMOTE_PORT", String.valueOf(req.remotePort));

		String queryString = req.getQueryString();
		env.put("QUERY_STRING", queryString != null ? queryString : "");
		env.put("PATH_INFO", req.getPathInfo());

		if(req.signatureHash != null) {
			env.put("AUTH_TYPE", req.authType);
			env.put("TLS_CLIENT_HASH", req.signatureHash);
			env.put("REMOTE_USER", req.subjectCn);
		}

		File f = new File(command);
		
		if( !f.isFile() || !f.canExecute()) {
			return null;
		}

		pb.directory(f.getParentFile());

		Process process = pb.start();

		InputStream procStdout = process.getInputStream();
		byte[] cgiResponse = procStdout.readAllBytes();

		return cgiResponse;
	}

	public static void main(String[] args) throws Exception {

		String command = "/home/kevin/eclipse-workspace/Jemserv/docroot/cgi-bin/test.sh";
		command = "/home/kevin/eclipse-workspace/Jemserv/docroot/cgi-bin/search";

		URL url = new URL("gemini://gemini.susa.net/cgi-bin/search?minetest");
		
		byte[] cgiResponse = CgiLauncher.launchCgiProcess(command, new GeminiRequest(url));

		System.out.println("Response\n");
		System.out.println(new String(cgiResponse, StandardCharsets.UTF_8));
	}
}
