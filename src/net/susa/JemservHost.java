package net.susa;

import java.util.ArrayList;

public class JemservHost {
	
	private ArrayList<String> hostnames;
	private String docRoot;
	private String cgiDir;
	
	/**
	 * A JemservHost must have a hostname and a document root.
	 * 
	 * @param primaryName the full hostname of the virtual host
	 * @param docRoot the location of the content directory
	 */
	public JemservHost(String primaryName, String docRoot) {
		
		hostnames = new ArrayList<>();
		hostnames.add(primaryName);
		
		if(docRoot.endsWith("/"))
			docRoot = docRoot.replaceFirst("/+$", "");

		this.docRoot = docRoot;
	}
	
	public void setCgiDir(String cgiDir) {
		
		// cgiDir must be relative to the root, and must have a component
		// after it (i.e. the executable), so it store e.g. "/cgi-bin/"
		if(cgiDir.startsWith("/") == false)
			cgiDir = "/" + cgiDir;
		if(cgiDir.endsWith("/") == false)
			cgiDir = cgiDir + "/";
		
		this.cgiDir = cgiDir;
	}
	
	public String getDocRoot() {
		return docRoot;
	}
	
	public String getCgiDir() {
		return cgiDir;
	}
	
	public void addHostname(String hostname) {
		if(hostnames.contains(hostname) == false)
			hostnames.add(hostname);
	}
	
	public boolean hasHostname(String hostname) {
		return hostnames.contains(hostname);
	}
}
