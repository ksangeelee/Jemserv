package net.susa;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.function.BiConsumer;

public class ServerConfig {

	private int geminiPort = 1965;
	private String geminiKeystore = "keystore.jks";
	private String geminiKeystorePassword = "password";
	
	public ServerConfig() throws Exception {

		System.out.println("System Properties:");
		ServerConfig.dumpProperties(System.getProperties());
		
		Properties props = new Properties();
		try {
			FileInputStream fis = new FileInputStream("config.properties");
			props.load(fis);
		} catch (FileNotFoundException fnfe) {
			System.out.println("Failed to load config. Current working directory is " + System.getProperty("user.dir"));
			System.out.println("Class path is " + System.getProperty("java.class.path"));
			throw new Exception("Aborting with no config");
		}
		
		System.out.println("Server Properties:");
		ServerConfig.dumpProperties(props);

		if(props.containsKey("gemini.port"))
			geminiPort = Integer.valueOf(props.getProperty("gemini.port"));
		if(props.containsKey("gemini.keystore"))
			geminiKeystore = props.getProperty("gemini.keystore").trim();
		if(props.containsKey("gemini.keystore.password"))
			geminiKeystorePassword = props.getProperty("gemini.keystore.password").trim();
	}
	
	public int getGeminiPort() {
		return geminiPort;
	}
	
	public String getGeminiKeystore() {
		return geminiKeystore;
	}

	public char[] getGeminiKeystorePassword() {
		return geminiKeystorePassword.toCharArray();
	}

	public static void dumpProperties(Properties props) {

		props.forEach(new BiConsumer<Object, Object>() {
			@Override
			public void accept(Object arg0, Object arg1) {
				System.out.println(" - " + arg0 + " = " + arg1);
			}
		});

	}
}
