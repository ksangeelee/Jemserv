package net.susa;

public class GeminiResponse {

	public int responseCode;
	public String meta;
	public byte[] content;
	public MimeType mimeType;
	
	public GeminiResponse(int responseCode) {
		this.responseCode = responseCode;
	}
	
	public GeminiResponse(int responseCode, String meta) {
		this(responseCode);
		this.meta = meta;
	}
	
	public GeminiResponse(int responseCode, byte[] content) {
		this(responseCode);
		this.content = content;
	}
	
	public GeminiResponse(int responseCode, byte[] content, MimeType mimeType) {
		this(responseCode, content);
		this.mimeType = mimeType;
	}
	
	public byte[] getResponseHeader() {
		
		if(responseCode == 20)
			return ("20 " + mimeType.contentType + "\r\n").getBytes();
		
		if(responseCode == 30 || responseCode == 31)
			return(Integer.toString(responseCode) + " " + meta + "\r\n").getBytes();
		
		if(responseCode == 51)
			return(Integer.toString(responseCode) + " " + meta + "\r\n").getBytes();
		
		return "50 PERMANENT FAILURE. Response code unknown to server.\r\n".getBytes();
			
	}
	
	public String guessMimeType() {
		
		if(content == null)
			return null;
		
		// png: 89 50 4E 47 0D 0A 1A 0A
		// jpg: FF D8 FF {DB|E0|EE|E1}
		if(content.length > 10) {
			if(		content[0] == 0xFF && 
					content[1] == 0xD8 &&
					content[2] == 0xFF &&
					(content[3] == 0xDB ||
					 content[3] == 0xE0 ||
					 content[3] == 0xEE ||
					 content[3] == 0xE1)
			) {
				return "image/jpeg"; 
			}
		}
		
		return null;
	}
	
	public static GeminiResponse redirect(String path) {
		return new GeminiResponse(31, path);
	}

	public static GeminiResponse permanentFailure(int failureCode) {
		return new GeminiResponse(failureCode, "NOT FOUND");
	}
	
	public static final int NOT_FOUND = 51;
}
