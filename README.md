# Jemserv Gemini Server in Java

> Warning: this code has been written in the pragmatic spirit of the Gemini project. If you are an OO or design-pattern purist, the software herein may cause a severe anxiety response.  

Java has a lot of its own terminology regarding SSL. It has different ways of using SSL, namely SSLSocketFactory and SSLEngine.

The keytool utility is used to manage keys and certificates. Keys and certificates are stored in a KeyStore, whereas TrustStore relates to trusted Certificate Authorities.

In short, the KeyStore holds the server's key and certificate, and the TrustStore is used to validate client certificates. 

The TrustStore already has a default configuration with the major Certificate Authorities, a bit like a web browser does. I think the only reason to bother with it is where you want to add your own CA.

The following is an library for Java that implements ByteChannel over TLS. It contains a lot of informative reading on the current state of SSL in Java. 

=> https://github.com/marianobarrios/tls-channel

## Converting PEM format keys to JKS format

Java KeyStore (JKS) is the format used by the Java world to store certificates and keys. According to Oracle's docs, the following can be used to convert a PEM certificate to JKS.

```
# First, export the PEM file using openssl
#
openssl pkcs12 -export -out mykey.pkcs12 -in mykey.pem
openssl pkcs12 -export -out mycert.pkcs12 -in mycert.pem

# Create an empty keystore by generating a key then deleting it (seems hacky!)
#
keytool -genkey -keyalg RSA -alias mykey -keystore mykeystore.ks
keytool -delete -alias mykey -keystore mykeystore.ks

# Import your private key into the empty JKS
#
keytool -v -importkeystore -srckeystore mykey.pkcs12 \
    -srcstoretype PKCS12 -destkeystore mykeystore.ks \
    -deststoretype JKS
``` 
=> https://docs.oracle.com/cd/E35976_01/server.740/es_admin/src/tadm_ssl_convert_pem_to_jks.html Oracle Docs: Converting PEM-format keys to JKS format

However, this looks inconsistent to me. Stack Exchange offers cleaner solutions. Note that the same JKS can be used for both keys/certs and for CAs. None of this is tested yet, I just note here for further investigation...

```
# If you need to add a CA
keytool -import -trustcacerts -alias root -file ca_someprovider.pem -keystore mykeystore.jks
# Now add the PEM key and cert (combined will have BEGIN_, END_ blocks)
cat cert.pem key.pem > combined.pem
keytool -import -trustcacerts -alias yourdomain -file combined.pem -keystore mykeystore.jks

# Similar, but using pkcs12 format files
# First concatenate all pem files (probably just use cert for this application)
cat *.pem > all.pem
openssl pkcs12 -export -inkey private.key -in all.pem -name test -out test.p12
keytool -importkeystore -srckeystore test.p12 -srcstoretype pkcs12 -destkeystore test.jks

```
## Absolute URLs

=> https://url.spec.whatwg.org/#absolute-url-with-fragment-string 

The spec states that a Gemini request is URL<CR><LF>, where URL is an absolute URL. According the whatwg.org, this means that the URL should be an <absolute-URL string>, which is made up of the following:

* <gemini>, followed by a <:>, followed by a <relative-URL>, and optionally <URL-query>

A <relative-URL> is one of the following three variants:

* scheme-relative-URL: //<opaque-host-and-port>, optionally <path-absolute-URL>
* path-absolute-URL: /<path-relative-URL>
* path-relative-scheme-less-URL: <path-relative-URL> (cannot start with 'gemini:')

An <opaque-host-and-port> is <hostname, ipv4, [ipv6]> (optional <:port>) or <empty string>

A <path-relative-URL> is zero or more <URL-path-segment>, separated (but not started) by '/'.

A <URL-path-segment> is zero or more <URL-unit> (characters), or a '.', or a '..'

Valid URL-units for gemini scheme:

* ASCII alphanumeric
* ! $ & ' ( ) * + , - . : ; = @ _ ~
* Code points U+00A0 to U+10FFFD

Curiously, by the above definition, gemini:/// is a valid URL (though not a valid gemini URL).

## SNI and server virtual hosts

The following code shows the essence of SNI matching. The matchers are used during the SSL handshake, and an exception is thrown if the SNI host given by the client does not match one of them.

However, I question the purpose of SNI being mandated in the Gemini spec. After all, it also mandates that requests are given using absolute URLs, so virtual hosting can be done at that point, regardless of what SNI host was given.

```
		SNIMatcher m = SNIHostName.createSNIMatcher(".+");
		ArrayList<SNIMatcher> matchers = new ArrayList<SNIMatcher>();
		matchers.add(m);
		SSLParameters p = serverSocket.getSSLParameters();
		p.setSNIMatchers(matchers);
		serverSocket.setSSLParameters(p);
```
